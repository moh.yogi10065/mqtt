// MQTT broker
const mosca = require('mosca')
const settings = {port: 1234}
const broker = new mosca.Server(settings)

// MySQL 
const mysql = require('mysql')
const db = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'MQTT_JS'
})

db.connect(()=>{
	console.log('Terhubung ke database !')
})

broker.on("ready", ()=>{
	console.log("Broker is ready")
})

broker.on("published", (packet)=>{
	message = packet.payload.toString()
	console.log(packet.payload.toString())
	if(message.slice(0,1) != '{' && message.slice(0,4) != 'mqtt'){
		const dbStat = `INSERT INTO data_sensor set ?`
		var data = {
			temperature: message
		}
		db.query(dbStat, data, (error,output)=>{
			if(error){
				console.log(error)
			}
			else{
				console.log("Data Telah tersimpan")
			}
		})
	}
})