var express = require("express");
const mosca = require('mosca')
const settings = {port: 1234}

const mqtt = require('mqtt');
const broker = new mosca.Server(settings)

var bodyParser = require("body-parser");
var app = express();

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }))

// MQTT CONNECT // 
broker.on("ready", ()=>{
	console.log("Broker is ready")
})

// MYSQL ///
var mysql = require('mysql')
var dbConn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'mydb'
})
dbConn.connect(()=>{
    console.log("Terhubung ke database")
})


app.get("/", (req, res)=>{
   res.send({error : false, message : "Selamat Datang"})
})

app.get('/data', (req,res)=>{
  dbConn.query(`SELECT * FROM db_data`, (error, results, fields)=>{
    return res.send({error: false, data: results, message: message});
  })
})

broker.on("published", (packet)=>{
	message = packet.payload.toString()
	console.log(packet.payload.toString())
	if(message.slice(0,1) != '{' && message.slice(0,4) != 'mqtt'){
		const dbStat = `INSERT INTO db_data set ?`
		var data = {
			temperature: message
		}
		db.query(dbStat, data, (error,output)=>{
			if(error){
				console.log(error)
			}
			else{
				console.log("Data Telah tersimpan")
			}
		})
	}
})

var server = app.listen(3000, ()=> {
    console.log("server running on port.", server.address().port);
});